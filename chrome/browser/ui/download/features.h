// Copyright 2021 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef IOS_CHROME_BROWSER_UI_DOWNLOAD_FEATURES_H_
#define IOS_CHROME_BROWSER_UI_DOWNLOAD_FEATURES_H_

#include "base/feature_list.h"

// Kill switch for Calendar support.
extern const base::Feature kCalendarKillSwitch;

// Kill switch for Vcard support.
extern const base::Feature kVCardKillSwitch;

// Kill switch for AR support.
extern const base::Feature kARKillSwitch;

#endif  // IOS_CHROME_BROWSER_UI_DOWNLOAD_FEATURES_H_
